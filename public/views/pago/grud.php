<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Datos de los pago</h5>
            </div>
            <div class="card-body">
                <form action="?c=pago&a=Guardar" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <input type="text" class="form-control" hidden name="id" value="<?php echo $pago->id; ?>">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>nombre del pago</label>
                                <input type="text" class="form-control" name="nombre" placeholder="Aceptacion" value="<?php echo $pago->nombre; ?>">
                            </div>
                        </div>
                        <div class="col-md-6 pl-1">
                            <div class="form-group">
                                <label>descripcion</label>
                                <input type="text" class="form-control" name="descripcion" placeholder="saludos...." value="<?php echo $pago->descripcion; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>cuota inicial</label>
                                <input type="number" class="form-control" name="cuotainicial" placeholder="Aceptacion" value="<?php echo $pago->cuotainicial; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>descuento</label>
                                <input type="number" class="form-control" name="descuento" placeholder="saludos...." value="<?php echo $pago->descuento; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>nro de cuotas</label>
                                <input type="number" class="form-control" name="cuotas" placeholder="saludos...." value="<?php echo $pago->cuotas; ?>">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>