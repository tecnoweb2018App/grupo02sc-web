
<div class="col-md-12">
    <div class="card">
        <a class="btn btn-primary" href="?c=pago&a=Crud" style="color: #e7ffee; text-decoration:none; ">Agregar</a>
        <div class="card-header">
            <h4 class="card-title"> Lista de pagos</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            nombres
                        </th>
                        <th>
                            descripcion
                        </th>
                        <th>
                            cuota inicial
                        </th>
                        <th>
                            descuento
                        </th>
                        <th>
                            nro cuotas
                        </th>
                        <th class="text-right">
                            Accion
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $i_pago): ?>
                        <tr>
                            <td><?php echo $i_pago->id; ?></td>
                            <td><?php echo $i_pago->nombre; ?></td>
                            <td><?php echo $i_pago->descripcion; ?></td>
                            <td><?php echo $i_pago->cuotainicial; ?></td>
                            <td><?php echo $i_pago->descuento; ?></td>
                            <td><?php echo $i_pago->cuotas; ?></td>                           
                            <td>
                                <a class="btn btn-info" href="?c=pago&a=Crud&id=<?php echo $i_pago->id; ?>">Actulizar</a>
                                <a class="btn btn-danger" href="?c=pago&a=Eliminar&id=<?php echo $i_pago->id; ?>">Eliminar</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>