<?php
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
?>
<div class="col-md-12">
    <div class="card">
        <a class="btn btn-primary" href="?c=registrar&a=Crud" style="color: #e7ffee; text-decoration:none; ">Agregar</a>
        <div class="card-header">
            <h4 class="card-title"> Lista de registrars</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                    <tr>
                        <th>
                            fecha
                        </th>
                        <th>
                            conferencia
                        </th>
                        <th>
                            estudiante
                        </th>
                        <th class="text-right">
                            Accion
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $i_registrar): ?>
                        <tr>
                            <td><?php echo $i_registrar->creado; ?></td>
                            <td><?php echo $i_registrar->idconferencia; ?></td>
                            <td><?php echo $i_registrar->idusuario; ?></td>                        
                            <td>
                                <a class="btn btn-info" href="?c=registrar&a=Crud&idusuarioold=<?php echo $i_registrar->idusuario; ?>&idconferenciaold=<?php echo $i_registrar->idconferencia; ?>">Actulizar</a>
                                <a class="btn btn-danger" href="?c=registrar&a=Eliminar&idusuario=<?php echo $i_registrar->idusuario; ?>&idconferencia=<?php echo $i_registrar->idconferencia; ?>">Eliminar</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>