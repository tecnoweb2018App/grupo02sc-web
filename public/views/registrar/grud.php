<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Datos de los registrar</h5>
            </div>
            <div class="card-body">
                <form action="?c=registrar&a=Guardar" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <input type="text" class="form-control" hidden name="idusuarioold" value="<?php echo $registrar->idusuarioold; ?>">
                        <input type="text" class="form-control" hidden name="idconferenciaold" value="<?php echo $registrar->idconferenciaold; ?>">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>idconferencia</label>
                                <input type="number" class="form-control" name="idconferencia" placeholder="idconferencia" value="<?php echo $registrar->idconferencia; ?>">
                            </div>
                        </div>
                        <div class="col-md-6 pl-1">
                            <div class="form-group">
                                <label>idusuario</label>
                                <input type="number" class="form-control" name="idusuario" placeholder="idusuario...." value="<?php echo $registrar->idusuario; ?>">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>