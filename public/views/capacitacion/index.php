
<div class="col-md-12">
    <div class="card">
        <a class="btn btn-primary" href="?c=capacitacion&a=Crud" style="color: #e7ffee; text-decoration:none; ">Agregar</a>
        <div class="card-header">
            <h4 class="card-title"> Lista de capacitaciones</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            aquien
                        </th>
                        <th>
                            titulacion
                        </th>
                        <th>
                            nombre
                        </th>
                        <th>
                            cargahoraria
                        </th>
                        <th>
                            inversion
                        </th>
                        <th>
                            fechainicio
                        </th>
                        <th>
                            tipo
                        </th>
                        <th>
                            categoria
                        </th>
                        <th>
                            gestion
                        </th>
                        <th class="text-right">
                            Accion
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $confe): ?>
                    <tr>
                        <td><?php echo $confe->id; ?></td>
                        <td><?php echo $confe->aquien; ?></td>
                        <td><?php echo $confe->titulacion; ?></td>
                        <td><?php echo $confe->nombre; ?></td>
                        <td><?php echo $confe->cargahoraria; ?></td>
                        <td><?php echo $confe->inversion; ?></td>
                        <td><?php echo $confe->fechainicio; ?></td>
                        <td><?php echo $confe->tipo; ?></td>
                        <td><?php echo $confe->categoria; ?></td>
                        <td><?php echo $confe->gestion; ?></td>
                        <td>
                            <a class="btn btn-info" href="?c=capacitacion&a=Crud&id=<?php echo $confe->id; ?>">Actulizar</a>
                            <a class="btn btn-danger" href="?c=capacitacion&a=Eliminar&id=<?php echo $confe->id; ?>">Eliminar</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>