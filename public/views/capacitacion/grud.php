<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Datos de las capacitacions</h5>
            </div>
            <div class="card-body">
                <form action="?c=capacitacion&a=Guardar" method="post" enctype="multipart/form-data">

                    <div class="row">
                        <input type="text" class="form-control" hidden name="id" placeholder="Titulos" value="<?php echo $capacitacions->id; ?>">
                        <div class="col-md-6 pl-1">
                            <div class="form-group">
                                <label>aquien</label>
                                <input type="text" class="form-control" name="aquien" placeholder="aquien" value="<?php echo $capacitacions->aquien; ?>">
                            </div>
                        </div>
                        <div class="col-md-6 pr-1">
                            <div class="form-group">
                                <label>Titulo de la capacitacion</label>
                                <input type="text" class="form-control" name="titulo" placeholder="Titulos" value="<?php echo $capacitacions->titulo; ?>">
                            </div>
                        </div>                       
                    </div>
                    <div class="row">
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>titulacion</label>
                                <input type="text" class="form-control" name="titulacion" placeholder="Ing" value="<?php echo $capacitacions->titulacion; ?>">
                            </div>
                        </div>
                        <div class="col-md-4 px-1">
                            <div class="form-group">
                                <label>nombre de la capacitacion</label>
                                <input type="text" class="form-control" name="nombre" placeholder="Country" value="<?php echo $capacitacions->nombre; ?>">
                            </div>
                        </div>
                        <div class="col-md-4 pl-1">
                            <div class="form-group">
                                <label>cargahoraria de la capacitacion</label>
                                <input type="number" class="form-control" name="cargahoraria" placeholder="11:30:00" value="<?php echo $capacitacions->cargahoraria; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>inversion</label>
                                <input type="number" class="form-control" name="inversion" placeholder="Ing" value="<?php echo $capacitacions->inversion; ?>">
                            </div>
                        </div>
                        <div class="col-md-4 px-1">
                            <div class="form-group">
                                <label>fechainicio de la capacitacion</label>
                                <input type="date" class="form-control" name="fechainicio" placeholder="Country" value="<?php echo $capacitacions->fechainicio; ?>">
                            </div>
                        </div>
                        <div class="col-md-4 pl-1">
                            <div class="form-group">
                                <label>tipo de la capacitacion</label>
                                <input type="number" class="form-control" name="tipo" placeholder="11:30:00" value="<?php echo $capacitacions->tipo; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>categoria</label>
                                <input type="number" class="form-control" name="categoria" placeholder="Ing" value="<?php echo $capacitacions->categoria; ?>">
                            </div>
                        </div>
                        <div class="col-md-4 px-1">
                            <div class="form-group">
                                <label>gestion de la capacitacion</label>
                                <input type="number" class="form-control" name="gestion" placeholder="Country" value="<?php echo $capacitacions->gestion; ?>">
                            </div>
                        </div>
                       
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>