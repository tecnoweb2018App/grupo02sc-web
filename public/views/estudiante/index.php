<?php
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
?>

<div class="col-md-12">
    <div class="card">
        <a href="?c=estudiante&a=Crud" class="btn btn-primary">Agregar</a>
        <div class="card-header">
            <h4 class="card-title"> Lista de estudiante</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            nombre
                        </th>
                        <th>
                            apellido
                        </th>
                        <th>
                            nacionalidad
                        </th>
                        <th>
                            fecha de nacimiento
                        </th>
                        <th>
                            lugar de nacimiento
                        </th>
                        <th>
                            cedula
                        </th>
                        <th>
                            calle
                        </th>
                        <th>
                            ciudad
                        </th>
                        <th>
                            telefono
                        </th>
                        <th>
                            correo
                        </th>
                        <th>
                            contrasena
                        </th>
                        <th>
                            calle trabajo
                        </th>
                        <th>
                            ciudad trabajo
                        </th>
                        <th>
                            telefono trabajo
                        </th>                        
                        <th>
                            correo trabajo
                        </th>
                        <th>
                            licenciatura
                        </th>
                        <th>
                            universidad
                        </th>
                        <th>
                            modalidad
                        </th>
                        <th>
                            otros etudios
                        </th>
                        <th class="text-right">
                            Accion
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $i_estudiante): ?>
                        <tr>
                            <td><?php echo $i_estudiante->id; ?></td>
                            <td><?php echo $i_estudiante->nombre; ?></td>
                            <td><?php echo $i_estudiante->apellido; ?></td>
                            <td><?php echo $i_estudiante->nacionalidad; ?></td>
                            <td><?php echo $i_estudiante->fechanacimiento; ?></td>
                            <td><?php echo $i_estudiante->lugarnacimiento; ?></td>
                            <td><?php echo $i_estudiante->cedula; ?></td>
                            <td><?php echo $i_estudiante->calle; ?></td>
                            <td><?php echo $i_estudiante->ciudad; ?></td>
                            <td><?php echo $i_estudiante->telefono; ?></td>
                            <td><?php echo $i_estudiante->correo; ?></td>
                            <td><?php echo $i_estudiante->contrasena; ?></td>
                            <td><?php echo $i_estudiante->calletrabajo; ?></td>
                            <td><?php echo $i_estudiante->ciudadtrabajo; ?></td>
                            <td><?php echo $i_estudiante->telefonotrabajo; ?></td>
                            <td><?php echo $i_estudiante->correotrabajo; ?></td>
                            <td><?php echo $i_estudiante->licenciatura; ?></td>
                            <td><?php echo $i_estudiante->universidad; ?></td>
                            <td><?php echo $i_estudiante->modalidad; ?></td>
                            <td><?php echo $i_estudiante->otrosestudios; ?></td>
                            <td>
                                <a class="btn btn-info" href="?c=estudiante&a=Crud&id=<?php echo $i_estudiante->id; ?>">Actulizar</a>
                                <a class="btn btn-danger" href="?c=estudiante&a=Eliminar&id=<?php echo $i_estudiante->id; ?>">Eliminar</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>