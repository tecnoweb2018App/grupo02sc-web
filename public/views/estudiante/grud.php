<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Datos de los estudiante</h5>
            </div>
            <div class="card-body">
                <form action="?c=estudiante&a=Guardar" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <input type="text" class="form-control" hidden name="id" value="<?php echo $estudiante->id; ?>">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>nombre del estudiante</label>
                                <input type="text" class="form-control" name="nombre" placeholder="nombre" value="<?php echo $estudiante->nombre; ?>">
                            </div>
                        </div>
                        <div class="col-md-6 pl-1">
                            <div class="form-group">
                                <label>apellido</label>
                                <input type="text" class="form-control" name="apellido" placeholder="apellidos...." value="<?php echo $estudiante->apellido; ?>">
                            </div>
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>nacionalidad</label>
                                <input type="text" class="form-control" name="nacionalidad" placeholder="boliviano" value="<?php echo $estudiante->nacionalidad; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>fechanacimiento</label>
                                <input type="date" class="form-control" name="fechanacimiento" placeholder=""    value="<?php echo $estudiante->fechanacimiento; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>lugarnacimiento</label>
                                <input type="text" class="form-control" name="lugarnacimiento" placeholder="lugarnacimiento...." value="<?php echo $estudiante->lugarnacimiento; ?>">
                            </div>
                        </div>                          
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>cedula</label>
                                <input type="number" class="form-control" name="cedula" placeholder="cedula" value="<?php echo $estudiante->cedula; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>calle</label>
                                <input type="text" class="form-control" name="calle" placeholder="calle...." value="<?php echo $estudiante->calle; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>ciudad</label>
                                <input type="text" class="form-control" name="ciudad" placeholder="ciudad...." value="<?php echo $estudiante->ciudad; ?>">
                            </div>
                        </div>                          
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>telefono</label>
                                <input type="number" class="form-control" name="telefono" placeholder="telefono" value="<?php echo $estudiante->telefono; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>correo</label>
                                <input type="email" class="form-control" name="correo" placeholder="correo...." value="<?php echo $estudiante->correo; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>contrasena</label>
                                <input type="password" class="form-control" name="contrasena" placeholder="contrasena...." value="<?php echo $estudiante->contrasena; ?>">
                            </div>
                        </div>                          
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>calle trabajo</label>
                                <input type="text" class="form-control" name="calletrabajo" placeholder="calle trabajo" value="<?php echo $estudiante->calletrabajo; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>ciudad trabajo</label>
                                <input type="text" class="form-control" name="ciudadtrabajo" placeholder="ciudad trabajo...." value="<?php echo $estudiante->ciudadtrabajo; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>telefono trabajo</label>
                                <input type="number" class="form-control" name="telefonotrabajo" placeholder="telefono trabajo...." value="<?php echo $estudiante->telefonotrabajo; ?>">
                            </div>
                        </div>                          
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>correo trabajo</label>
                                <input type="email" class="form-control" name="correotrabajo" placeholder="correo trabajo" value="<?php echo $estudiante->correotrabajo; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>licenciatura</label>
                                <input type="text" class="form-control" name="licenciatura" placeholder="licenciatura...." value="<?php echo $estudiante->licenciatura; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>universidad</label>
                                <input type="universidad" class="form-control" name="universidad" placeholder="universidad...." value="<?php echo $estudiante->universidad; ?>">
                            </div>
                        </div>                          
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>modalidad</label>
                                <input type="text" class="form-control" name="modalidad" placeholder="modalidad" value="<?php echo $estudiante->modalidad; ?>">
                            </div>
                        </div>
                        <div class="col-md-6 pl-1">
                            <div class="form-group">
                                <label>otros estudios</label>
                                <input type="text" class="form-control" name="otrosestudios" placeholder="otros estudios...." value="<?php echo $estudiante->otrosestudios; ?>">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>