
<div class="col-md-12">
    <div class="card">
        <a class="btn btn-primary" href="?c=usuario&a=Crud" style="color: #e7ffee; text-decoration:none; ">Agregar</a>
        <div class="card-header">
            <h4 class="card-title"> Lista de usuarios</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            nombre
                        </th>
                        <th>
                            apellido
                        </th>
                        <th>
                            nacionalidad
                        </th>
                        <th>
                            fecha de nacimiento
                        </th>
                        <th>
                            lugar de nacimiento
                        </th>
                        <th>
                            cedula
                        </th>
                        <th>
                            calle
                        </th>
                        <th>
                            ciudad
                        </th>
                        <th>
                            telefono
                        </th>
                        <th>
                            correo
                        </th>
                        <th>
                            contrasena
                        </th>
                        <th>
                            tipo
                        </th>
                        <th class="text-right">
                            Accion
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $i_usuario): ?>
                        <tr>
                            <td><?php echo $i_usuario->id; ?></td>
                            <td><?php echo $i_usuario->nombre; ?></td>
                            <td><?php echo $i_usuario->apellido; ?></td>
                            <td><?php echo $i_usuario->nacionalidad; ?></td>
                            <td><?php echo $i_usuario->fechanacimiento; ?></td>
                            <td><?php echo $i_usuario->lugarnacimiento; ?></td>
                            <td><?php echo $i_usuario->cedula; ?></td>
                            <td><?php echo $i_usuario->calle; ?></td>
                            <td><?php echo $i_usuario->ciudad; ?></td>
                            <td><?php echo $i_usuario->telefono; ?></td>
                            <td><?php echo $i_usuario->correo; ?></td>
                            <td><?php echo $i_usuario->contrasena; ?></td>
                            <td><?php echo $res=($i_usuario->tipo==1)? "administrador":"transcriptor"; ?></td>
                            <td>
                                <a class="btn btn-info" href="?c=usuario&a=Crud&id=<?php echo $i_usuario->id; ?>">Actulizar</a>
                                <a class="btn btn-danger" href="?c=usuario&a=Eliminar&id=<?php echo $i_usuario->id; ?>">Eliminar</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>