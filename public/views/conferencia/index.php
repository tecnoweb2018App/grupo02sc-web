
<div class="col-md-12">
    <div class="card">
        <a class="btn btn-primary" href="?c=conferencia&a=Crud" style="color: #e7ffee; text-decoration:none; ">Agregar</a>
        <div class="card-header">
            <h4 class="card-title"> Lista de conferencias</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Titulo
                        </th>
                        <th>
                            Descripcion
                        </th>
                        <th>
                            Disertante
                        </th>
                        <th>
                            Hora
                        </th>
                        <th class="text-right">
                            Fecha
                        </th>
                        <th class="text-right">
                            Accion
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $confe): ?>
                    <tr>
                        <td><?php echo $confe->id; ?></td>
                        <td><?php echo $confe->titulo; ?></td>
                        <td><?php echo $confe->descripcion; ?></td>
                        <td><?php echo $confe->disertante; ?></td>
                        <td><?php echo $confe->fecha; ?></td>
                        <td class="text-right"><?php echo $confe->hora; ?></td>
                        <td>
                            <a class="btn btn-info" href="?c=conferencia&a=Crud&id=<?php echo $confe->id; ?>">Actulizar</a>
                            <a class="btn btn-danger" href="?c=conferencia&a=Eliminar&id=<?php echo $confe->id; ?>">Eliminar</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>