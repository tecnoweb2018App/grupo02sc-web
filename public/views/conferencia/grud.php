<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Datos de las conferencias</h5>
            </div>
            <div class="card-body">
                <form action="?c=conferencia&a=Guardar" method="post" enctype="multipart/form-data">

                    <div class="row">
                                <input type="text" class="form-control" hidden name="id" placeholder="Titulos" value="<?php echo $conferencias->id; ?>">
                        <div class="col-md-6 pr-1">
                            <div class="form-group">
                                <label>Titulo de la conferencia</label>
                                <input type="text" class="form-control" name="titulo" placeholder="Titulos" value="<?php echo $conferencias->titulo; ?>">
                            </div>
                        </div>
                        <div class="col-md-6 pl-1">
                            <div class="form-group">
                                <label>Descripcion</label>
                                <input type="text" class="form-control" name="descripcion" placeholder="Descripcion" value="<?php echo $conferencias->descripcion; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>Disertante</label>
                                <input type="text" class="form-control" name="disertante" placeholder="Ing" value="<?php echo $conferencias->disertante; ?>">
                            </div>
                        </div>
                        <div class="col-md-4 px-1">
                            <div class="form-group">
                                <label>fecha de la conferencia</label>
                                <input type="date" class="form-control" name="fecha" placeholder="Country" value="<?php echo $conferencias->fecha; ?>">
                            </div>
                        </div>
                        <div class="col-md-4 pl-1">
                            <div class="form-group">
                                <label>Hora de la conferencia</label>
                                <input type="text" class="form-control" name="hora" placeholder="11:30:00" value="<?php echo $conferencias->hora; ?>">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>