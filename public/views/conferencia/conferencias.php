
<div class="content">
    <div class="row">
        <?php foreach($this->model->ListarInvertido() as $confe): ?>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <?php echo $confe->id; ?>
                    <h2 class="card-title"><?php echo $confe->titulo; ?></h2>
                </div>
                <div class="card-body">
                    <p><?php echo $confe->descripcion; ?></p>
                    <p><strong><?php echo $confe->disertante; ?></strong></p>                    
                    <p>fecha: <?php echo $confe->fecha; ?></p>
                    <p>hora: <?php echo $confe->hora; ?></p>
                </div>
                <div class="card-footer">
                    <span class="bg-primary text-white p-2">suscripto</span>
                    <button class="btn btn-success">Suscribirse</button>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>


                    