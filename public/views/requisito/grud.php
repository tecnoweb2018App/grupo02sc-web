<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Datos de los requisito</h5>
            </div>
            <div class="card-body">
                <form action="?c=requisito&a=Guardar" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <input type="text" class="form-control" hidden name="id" value="<?php echo $requisito->id; ?>">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>nombre del requisito</label>
                                <input type="text" class="form-control" name="nombre" placeholder="Aceptacion" value="<?php echo $requisito->nombre; ?>">
                            </div>
                        </div>
                        <div class="col-md-6 pl-1">
                            <div class="form-group">
                                <label>descripcion</label>
                                <input type="text" class="form-control" name="descripcion" placeholder="saludos...." value="<?php echo $requisito->descripcion; ?>">
                            </div>
                        </div>
                        
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>