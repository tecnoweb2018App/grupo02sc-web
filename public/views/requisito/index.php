
<div class="col-md-12">
    <div class="card">
        <a class="btn btn-primary"  href="?c=requisito&a=Crud" style="color: #e7ffee; text-decoration:none; ">Agregar</a>
        <div class="card-header">
            <h4 class="card-title"> Lista de requisitos</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            nombres
                        </th>
                        <th>
                            descripcion
                        </th>
                        
                        <th class="text-right">
                            Accion
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $i_requisito): ?>
                        <tr>
                            <td><?php echo $i_requisito->id; ?></td>
                            <td><?php echo $i_requisito->nombre; ?></td>
                            <td><?php echo $i_requisito->descripcion; ?></td>
                           
                            <td>
                                <a class="btn btn-info" href="?c=requisito&a=Crud&id=<?php echo $i_requisito->id; ?>">Actulizar</a>
                                <a class="btn btn-danger" href="?c=requisito&a=Eliminar&id=<?php echo $i_requisito->id; ?>">Eliminar</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>