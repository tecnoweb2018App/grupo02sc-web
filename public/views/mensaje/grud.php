<div class="content">
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Datos de los mensajes</h5>
            </div>
            <div class="card-body">
                <form action="?c=mensaje&a=Guardar" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <input type="text" class="form-control" hidden name="id" value="<?php echo $mensaje->id; ?>">
                        <div class="col-md-12 pr-1">
                            <div class="form-group">
                                <label>Asunto del mensaje</label>
                                <input type="text" class="form-control" name="asunto" placeholder="Aceptacion" value="<?php echo $mensaje->asunto; ?>">
                            </div>
                        </div>
                        <div class="col-md-12 pr-1">
                            <div class="form-group">
                                <label>Contenido</label>
                                <input type="text" class="form-control" name="contenido" placeholder="saludos...." value="<?php echo $mensaje->contenido; ?>">
                            </div>
                        </div>
                        <div class="col-md-12 pr-1">
                            <div class="form-group">
                                <label>Tipo de mensaje</label>
                                
                                <select name="tipo" class="form-control" value="<?php echo isset($mensaje->tipo)? $mensaje->tipo:1; ?>">
                                    <option value="1">alerta</option>
                                    <option value="2">mensaje</option>
                                 </select>
                            </div>
                        </div>
                        <div class="col-md-12 pr-1">
                            <div class="form-group">
                                <label>Enviar a</label>
                                <!--<input type="number" class="form-control" name="tipo" placeholder="Informativo" value="<?php echo $mensaje->tipo; ?>"> -->
                                 <select name="opcion" id="selectOpcion" class="form-control" value="1">
                                    <option value="1">todos</option>
                                    <option value="2">todos estudiante</option>
                                    <option value="3">todos administrador</option>
                                    <option value="4">especificar</option>
                                 </select>
                            </div>
                            <div class="form-group" id="inputDestino" style="display:none;">
                                <label>Destino</label>
                                <input type="text" name="correo" placeholder="correo@gmail.com" class="form-control">                                       
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>
<script>
console.log("enviando mensaje");
$(document).ready(function(){
    $("#selectOpcion").change(function(){
        console.log(this.value);
        if (this.value==4){
            //$("#inputDestino").show();
            console.log("mostrar");
            document.getElementById("inputDestino").style.display = "block";            
        }else{
            //$("#inputDestino").hidden();
            console.log("ocultar");
            document.getElementById("inputDestino").style.display = "none";            
        }
    });
});
</script>