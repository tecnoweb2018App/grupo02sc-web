<div class="sidebar" data-color="orange">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
            CT
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
            Creative Tim
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li>
                <a href="">
                    <i class="now-ui-icons design_app"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a href="?c=estudiante">
                    <i class="now-ui-icons design_app"></i>
                    <p>Estudiantes</p>
                </a>
            </li>
            <li>
                <a href="?c=capacitacion">
                    <i class="now-ui-icons design_app"></i>
                    <p>capacitacion</p>
                </a>
            </li>
            <li>
                <a href="?c=mensaje">
                    <i class="now-ui-icons design_app"></i>
                    <p>mensajes</p>
                </a>
            </li>
            <li>
                <a href="?c=registrar">
                    <i class="now-ui-icons design_app"></i>
                    <p>registrar</p>
                </a>
            </li>
            <li>
                <a href="?c=pago">
                    <i class="now-ui-icons education_atom"></i>
                    <p>Pagos</p>
                </a>
            </li>
            <li>
                <a href="?c=usuario">
                    <i class="now-ui-icons education_atom"></i>
                    <p>Usuarios</p>
                </a>
            </li>
            <li>
                <a href="?c=conferencia">
                    <i class="now-ui-icons education_atom"></i>
                    <p>Conferencia</p>
                </a>
            </li>
            <li>
                <a href="?c=mensaje">
                    <i class="now-ui-icons location_map-big"></i>
                    <p>Mensajes</p>
                </a>
            </li>
            <li>
                <a href="?c=horario">
                    <i class="now-ui-icons education_atom"></i>
                    <p>horario</p>
                </a>
            </li>
            <li>
                <a href="?c=requisito">
                    <i class="now-ui-icons education_atom"></i>
                    <p>requisito</p>
                </a>
            </li>
            <li>
                <a href="./notifications.html">
                    <i class="now-ui-icons ui-1_bell-53"></i>
                    <p>Notifications</p>
                </a>
            </li>
            <li>
                <a href="./user.html">
                    <i class="now-ui-icons users_single-02"></i>
                    <p>User Profile</p>
                </a>
            </li>
            <li>
                <a href="./tables.html">
                    <i class="now-ui-icons design_bullet-list-67"></i>
                    <p>Table List</p>
                </a>
            </li>
            <li>
                <a href="./typography.html">
                    <i class="now-ui-icons text_caps-small"></i>
                    <p>Typography</p>
                </a>
            </li>
            <li class="active-pro">
                <a href="./upgrade.html">
                    <i class="now-ui-icons arrows-1_cloud-download-93"></i>
                    <p>Upgrade to PRO</p>
                </a>
            </li>
        </ul>
    </div>
</div>
