
<div class="col-md-12">
    <div class="card">
        <a class="btn btn-primary" href="?c=horario&a=Crud" style="color: #e7ffee; text-decoration:none; ">Agregar</a>
        <div class="card-header">
            <h4 class="card-title"> Lista de horarios</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            DIA
                        </th>
                        <th>
                            Hora Inicio
                        </th>
                        <th>
                            Hora Fin
                        </th>
                        <th class="text-right">
                            Accion
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $i_hora): ?>
                    <tr>
                        <td><?php echo $i_hora->id; ?></td>
                        <td><?php echo $i_hora->dia; ?></td>
                        <td><?php echo $i_hora->horarioinicial; ?></td>
                        <td><?php echo $i_hora->horariofinal; ?></td>
                        
                        <td>
                            <a class="btn btn-info" href="?c=horario&a=Crud&id=<?php echo $i_hora->id; ?>">Actulizar</a>
                            <a class="btn btn-danger" href="?c=horario&a=Eliminar&id=<?php echo $i_hora->id; ?>">Eliminar</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>