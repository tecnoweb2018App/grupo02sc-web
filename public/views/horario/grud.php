    <div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5 class="title">Datos de las horario</h5>
            </div>
            <div class="card-body">
                <form action="?c=horario&a=Guardar" method="post" enctype="multipart/form-data">

                    <div class="row">
                        <input type="text" class="form-control" hidden name="id" placeholder="Titulos" value="<?php echo $horario->id; ?>">
                        
                        <div class="col-md-6 pr-1">
                            <div class="form-group">
                                <label>dia</label>
                                <select class="form-control" name="dia" value="<?php echo $horario->dia;?>">
                                    <option value="LUNES">LUNES</option>
                                    <option value="MARTES">MARTES</option>
                                    <option value="MIERCOLES">MIERCOLES</option>
                                    <option value="JUEVES">JUEVES</option>
                                    <option value="VIERNES">VIERNES</option>
                                    <option value="SABADO" >SABADO</option>
                                    <option value="DOMINGO">DOMINGO</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-4 pl-1">
                            <div class="form-group">
                                <label>Hora de inicio</label>
                                <input type="time" class="form-control" name="horarioinicial" placeholder="11:30:00" value="<?php echo $horario->horarioinicial; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-4 pl-1">
                            <div class="form-group">
                                <label>Hora de finilizacion</label>
                                <input type="time" class="form-control" name="horariofinal" placeholder="11:30:00" value="<?php echo $horario->horariofinal; ?>">
                            </div>
                        </div>
                      
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>