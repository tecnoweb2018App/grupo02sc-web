<?php
include_once('core/db.php');

class pago
{
    private $pdo;

    public $id;
    public $nombre;
    public $descripcion;
    public $cuotainicial;
    public $descuento;
    public $cuotas;



    public function __CONSTRUCT()
    {
        try
        {
            $this->pdo = Database::StartUp();
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT id, nombre, descripcion,cuotainicial,descuento,cuotas FROM pago");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM pago WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM pago WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE pago SET 
						nombre          = ?,
						descripcion		= ?,
                        cuotainicial	= ?,
                        descuento		= ?,
                        cuotas		    = ?
				    	WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->descripcion,
                        $data->cuotainicial,
                        $data->descuento,
                        $data->cuotas,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(pago $data)
    {
        try
        {
            $sql = "INSERT INTO pago (nombre, descripcion,cuotainicial,descuento,cuotas) VALUES (?, ?,?,?,?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->descripcion,
                        $data->cuotainicial,
                        $data->descuento,
                        $data->cuotas
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
