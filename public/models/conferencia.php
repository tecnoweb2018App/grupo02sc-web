<?php
include_once('core/db.php');

class conferencias
{
    private $pdo;

    public $id;
    public $titulo;
    public $descripcion;
    public $disertante;
    public $fecha;
    public $hora;



    public function __CONSTRUCT()
    {
        try
        {
            $this->pdo = Database::StartUp();
        }
        catch(Exception $e)
        {                                                           
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT id, titulo, descripcion, disertante, fecha, hora FROM conferencias");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function ListarInvertido()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT id, titulo, descripcion, disertante, fecha, hora FROM conferencias  ORDER BY id desc");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM conferencias WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM conferencias WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE conferencias SET 
						titulo          = ?,
						descripcion		= ?,
						disertante	    =?,
						fecha           =?,
						hora            =?
				    	WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->titulo,
                        $data->descripcion,
                        $data->disertante,
                        $data->fecha,
                        $data->hora,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(Conferencias $data)
    {
        try
        {
            $sql = "INSERT INTO conferencias (titulo, descripcion, disertante, fecha, hora) VALUES (?, ?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->titulo,
                        $data->descripcion,
                        $data->disertante,
                        $data->fecha,
                        $data->hora

                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
