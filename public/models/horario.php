<?php
include_once('core/db.php');

class horario
{
    private $pdo;

    public $id;
    public $dia;
    public $horarioinicial;
    public $horariofinal;



    public function __CONSTRUCT()
    {
        try
        {
            $this->pdo = Database::StartUp();
        }
        catch(Exception $e)
        {                                                           
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT id, dia,horarioinicial,horariofinal FROM horario");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }                                                                                                                                                                                                                                                                                                                                                                               

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM horario WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM horario WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE horario SET 
						dia          = ?,
						horarioinicial	= ?,
				        horariofinal    =?
				    	WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->dia,
                        $data->horarioinicial,
                        $data->horariofinal,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(Horario $data)
    {
        try
        {
            $sql = "INSERT INTO horario (dia, horarioinicial,horariofinal) VALUES (?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->dia,
                        $data->horarioinicial,
                        $data->horariofinal
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
