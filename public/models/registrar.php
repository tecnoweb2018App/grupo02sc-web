<?php
include_once('core/db.php');

class registrar
{
    private $pdo;

    public $idconferencia;
    public $idusuario;
    public $creado;

    public function __CONSTRUCT()
    {
        try
        {
            $this->pdo = Database::StartUp();
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM reguistrarse");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Obtener($idusuario,$idconferencia)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM reguistrarse WHERE idusuario = ? and idconferencia=? ");


            $stm->execute(array($idusuario,$idconferencia));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($idusuario,$idconferencia)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM registrar WHERE idconferencia = ? and idusuario=?");

            $stm->execute(array($idconferencia,$idusuario));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data,$usuarioold,$conferenciaold)
    {
        try
        {
            $sql = "UPDATE reguistrarse SET 
						idusuario       = ? ,
                        idconferencia   = ?
				    	WHERE idusuario =? and idconferencia	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->idusuario,
                        $data->idconferencia,
                        $usuarioold,
                        $conferenciaold
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(registrar $data)
    {
        try
        {
            $sql = "INSERT INTO reguistrarse (idusuario, idconferencia) VALUES (?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->idusuario,
                        $data->idconferencia
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
