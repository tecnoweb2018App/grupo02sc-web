<?php
include_once('core/db.php');

class busqueda
{
    private $pdo;

    public $id;
    public $aquien;
    public $titulacion;
    public $nombre;
    public $cargahoraria;
    public $inversion;
    public $fechinicio;
    public $tipo;
    public $categoria;
    public $gestion;

    public function __CONSTRUCT()
    {
        try
        {
            $this->pdo = Database::StartUp();
        }
        catch(Exception $e)
        {                                                           
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM busqueda");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM busqueda WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM busqueda WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE busqueda SET 
						aquien        = ?,
						titulacion	  = ?,
						nombre	      =?,
						cargahoraria  =?,
						inversion     =?,
                        fechainicio   =?,
                        tipo          =?,
                        categoria     =?,
                        gestion       =?
				    	WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->aquien,
                        $data->titulacion,
                        $data->nombre,
                        $data->cargahoraria,
                        $data->inversion,
                        $data->fechainicio,
                        $data->tipo,
                        $data->categoria,
                        $data->gestion,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(busqueda $data)
    {
        try
        {
            $sql = "INSERT INTO busqueda (aquien, titulacion, nombre, cargahoraria, inversion,fechainicio,tipo,categoria,gestion) VALUES (?, ?, ?, ?, ?,?,?,?,?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->aquien,
                        $data->titulacion,
                        $data->nombre,
                        $data->cargahoraria,
                        $data->inversion,
                        $data->fechainicio,
                        $data->tipo,
                        $data->categoria,
                        $data->gestion
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
