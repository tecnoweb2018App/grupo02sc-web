<?php
include_once('core/db.php');

class estudiante
{
    private $pdo;

    public $id;
    public $nombre;
    public $apellido;
    public $nacionalidad;
    public $fechanacimiento;
    public $lugarnacimiento;

    public $cedula;
    public $calle;
    public $ciudad;
    public $telefono;
    public $correo;

    public $contrasena;
    public $calletrabajo;
    public $ciudadtrabajo;
    public $telefonotrabajo;
    public $correotrabajo;
    public $licenciatura;

    public $universidad;
    public $modalidad;
    public $otrosestudios;


    public function __CONSTRUCT()
    {
        try
        {
            $this->pdo = Database::StartUp();
        }
        catch(Exception $e)
        {                                                           
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try
        {
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM usuario where tipo=3");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }                                                                                                                                                                                                                                                                                                                                                                               

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM usuario WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM usuario WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE usuario SET 
						nombre          = ?,
						apellido	    = ?,
				        nacionalidad    =?,
                        fechanacimiento =?,
                        lugarnacimiento =?,
                        cedula          =?,
                        calle           =?,
                        ciudad          =?,
                        telefono        =?,
                        correo          =?,
                        contrasena      =?,
                        calletrabajo    =?,
                        ciudadtrabajo   =?,
                        telefonotrabajo =?,
                        correotrabajo   =?,
                        licenciatura    =?,
                        universidad     =?,
                        modalidad       =?,
                        otrosestudios   =?
				    	WHERE id	    = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->nacionalidad,
                        $data->fechanacimiento,
                        $data->lugarnacimiento,

                        $data->cedula,
                        $data->calle,
                        $data->ciudad,
                        $data->telefono,
                        $data->correo,

                        $data->contrasena,
                        $data->calletrabajo,
                        $data->ciudadtrabajo,
                        $data->telefonotrabajo,
                        $data->correotrabajo,
                        $data->licenciatura,

                        $data->universidad,
                        $data->modalidad,
                        $data->otrosestudios,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    public function Registrar(estudiante $data)
    {
        try
        {
            $sql = "INSERT INTO usuario (nombre, apellido,nacionalidad,fechanacimiento,lugarnacimiento,cedula,calle,ciudad,telefono,correo,contrasena,calletrabajo,ciudadtrabajo,telefonotrabajo,correotrabajo,licenciatura,universidad,modalidad,otrosestudios) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->nacionalidad,
                        $data->fechanacimiento,
                        $data->lugarnacimiento,

                        $data->cedula,
                        $data->calle,
                        $data->ciudad,
                        $data->telefono,
                        $data->correo,

                        $data->contrasena,
                        $data->calletrabajo,
                        $data->ciudadtrabajo,
                        $data->telefonotrabajo,
                        $data->correotrabajo,
                        $data->licenciatura,

                        $data->universidad,
                        $data->modalidad,
                        $data->otrosestudios
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
