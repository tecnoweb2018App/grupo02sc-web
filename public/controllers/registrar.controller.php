<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 15-11-18
 * Time: 08:16 PM
 */

require_once 'models/registrar.php';


class registrarController
{
    public function __CONSTRUCT(){
        $this->model = new registrar();
    }


    public function Index(){
        require_once 'views/registrar/index.php';

    }

    public function Crud(){
        $registrar = new registrar();

        if(isset($_REQUEST['idusuarioold'])&& isset($_REQUEST['idconferenciaold'])){
            $registrar = $this->model->Obtener($_REQUEST['idusuarioold'],$_REQUEST['idconferenciaold']);
        }

        require_once 'views/registrar/grud.php';

    }


    public function Guardar(){
        $registrar = new registrar();

        $registrar->idusuario = $_REQUEST['idusuario'];
        $registrar->idconferencia = $_REQUEST['idconferencia'];
        
        ($_REQUEST['idusuarioold'] > 0 && $_REQUEST['idconferenciaold'] > 0)? 
            $this->model->Actualizar($registrar,$_REQUEST['idusuarioold'],$_REQUEST['idconferenciaold']):
            $this->model->Registrar($registrar);

        require_once 'views/registrar/index.php';
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['idusuario'], $_REQUEST['idconferencia']);
        require_once 'views/registrar/index.php';
    }


}