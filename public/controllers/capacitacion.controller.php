<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 15-11-18
 * Time: 08:16 PM
 */

require_once 'models/capacitacion.php';


class capacitacionController
{
    public function __CONSTRUCT(){
        $this->model = new capacitacion();
    }


    public function Index(){
        require_once 'views/capacitacion/index.php';

    }

    public function Crud(){
        $capacitacion = new capacitacion();

        if(isset($_REQUEST['id'])){
            $capacitacion = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'views/capacitacion/grud.php';

    }


    public function Guardar(){
        $capacitacion = new capacitacion();

        $capacitacion->id = $_REQUEST['id'];
        $capacitacion->aquien = $_REQUEST['aquien'];
        $capacitacion->titulacion = $_REQUEST['titulacion'];
        $capacitacion->nombre = $_REQUEST['nombre'];
        $capacitacion->cargahoraria = $_REQUEST['cargahoraria'];
        $capacitacion->inversion = $_REQUEST['inversion'];
        $capacitacion->fechainicio = $_REQUEST['fechainicio'];
        $capacitacion->tipo = $_REQUEST['tipo'];
        $capacitacion->categoria = $_REQUEST['categoria'];
        $capacitacion->gestion = $_REQUEST['gestion'];

        $capacitacion->id > 0
            ? $this->model->Actualizar($capacitacion)
            : $this->model->Registrar($capacitacion);

        require_once 'views/capacitacion/index.php';
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        require_once 'views/capacitacion/index.php';
    }


}