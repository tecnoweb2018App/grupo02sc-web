<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 15-11-18
 * Time: 08:16 PM
 */

require_once 'models/mensaje.php';


class mensajeController
{
    public function __CONSTRUCT(){
        $this->model = new mensaje();
    }


    public function Index(){
        require_once 'views/mensaje/index.php';

    }

    public function Crud(){
        $mensaje = new mensaje();

        if(isset($_REQUEST['id'])){
            $mensaje = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'views/mensaje/grud.php';

    }


    public function Guardar(){
        $mensaje = new mensaje();

        $mensaje->id = $_REQUEST['id'];
        $mensaje->asunto = $_REQUEST['asunto'];
        $mensaje->contenido = $_REQUEST['contenido'];
        $mensaje->tipo = $_REQUEST['tipo'];

        $mensaje->id > 0
            ? $this->model->Actualizar($mensaje)
            : $this->model->Registrar($mensaje);

        require_once 'views/mensaje/index.php';
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        require_once 'views/mensaje/index.php';
    }
    public function enviar(){
        require_once 'views/mensaje/enviar.php';
    }

    public function xhrListar(){
        echo $this->model->xhrListar();
    }

}