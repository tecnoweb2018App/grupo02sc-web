<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 15-11-18
 * Time: 08:16 PM
 */

require_once 'models/busqueda.php';


class busquedaController
{
    public function __CONSTRUCT(){
        $this->model = new busqueda();
    }


    public function Index(){
        require_once 'views/busqueda/index.php';

    }

    public function Crud(){
        $busqueda = new busqueda();

        if(isset($_REQUEST['id'])){
            $busqueda = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'views/busqueda/grud.php';

    }


    public function Guardar(){
        $busqueda = new busqueda();

        $busqueda->id = $_REQUEST['id'];
        $busqueda->aquien = $_REQUEST['aquien'];
        $busqueda->titulacion = $_REQUEST['titulacion'];
        $busqueda->nombre = $_REQUEST['nombre'];
        $busqueda->cargahoraria = $_REQUEST['cargahoraria'];
        $busqueda->inversion = $_REQUEST['inversion'];
        $busqueda->fechainicio = $_REQUEST['fechainicio'];
        $busqueda->tipo = $_REQUEST['tipo'];
        $busqueda->categoria = $_REQUEST['categoria'];
        $busqueda->gestion = $_REQUEST['gestion'];

        $busqueda->id > 0
            ? $this->model->Actualizar($busqueda)
            : $this->model->Registrar($busqueda);

        require_once 'views/busqueda/index.php';
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        require_once 'views/busqueda/index.php';
    }


}