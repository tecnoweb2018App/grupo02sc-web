<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 15-11-18
 * Time: 08:16 PM
 */

require_once 'models/requisito.php';


class requisitoController
{
    public function __CONSTRUCT(){
        $this->model = new requisito();
    }


    public function Index(){
        require_once 'views/requisito/index.php';

    }

    public function Crud(){
        $requisito = new requisito();

        if(isset($_REQUEST['id'])){
            $requisito = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'views/requisito/grud.php';

    }


    public function Guardar(){
        $requisito = new requisito();

        $requisito->id = $_REQUEST['id'];
        $requisito->nombre = $_REQUEST['nombre'];
        $requisito->descripcion = $_REQUEST['descripcion'];

        $requisito->id > 0
            ? $this->model->Actualizar($requisito)
            : $this->model->Registrar($requisito);

        require_once 'views/requisito/index.php';
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        require_once 'views/requisito/index.php';
    }


}