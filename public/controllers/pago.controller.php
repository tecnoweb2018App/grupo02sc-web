<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 15-11-18
 * Time: 08:16 PM
 */

require_once 'models/pago.php';


class pagoController
{
    public function __CONSTRUCT(){
        $this->model = new pago();
    }


    public function Index(){
        require_once 'views/pago/index.php';

    }

    public function Crud(){
        $pago = new pago();

        if(isset($_REQUEST['id'])){
            $pago = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'views/pago/grud.php';

    }


    public function Guardar(){
        $pago = new pago();

        $pago->id = $_REQUEST['id'];
        $pago->nombre = $_REQUEST['nombre'];
        $pago->descripcion = $_REQUEST['descripcion'];
        $pago->cuotainicial = $_REQUEST['cuotainicial'];
        $pago->descuento = $_REQUEST['descuento'];
        $pago->cuotas = $_REQUEST['cuotas'];

        $pago->id > 0
            ? $this->model->Actualizar($pago)
            : $this->model->Registrar($pago);

        require_once 'views/pago/index.php';
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        require_once 'views/pago/index.php';
    }


}