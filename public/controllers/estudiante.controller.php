<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 15-11-18
 * Time: 08:16 PM
 */

require_once 'models/estudiante.php';


class estudianteController
{
    public function __CONSTRUCT(){
        $this->model = new estudiante();
    }


    public function Index(){
        require_once 'views/estudiante/index.php';
    }

    public function Crud(){
        $estudiante = new estudiante();

        if(isset($_REQUEST['id'])){
            $estudiante = $this->model->Obtener($_REQUEST['id']);
        }
        require_once 'views/estudiante/grud.php';
    }


    public function Guardar(){
        $estudiante = new estudiante();

        $estudiante->id = $_REQUEST['id'];
        $estudiante->nombre = $_REQUEST['nombre'];
        $estudiante->apellido = $_REQUEST['apellido'];
        $estudiante->nacionalidad = $_REQUEST['nacionalidad'];
        $estudiante->fechanacimiento = $_REQUEST['fechanacimiento'];
        $estudiante->lugarnacimiento = $_REQUEST['lugarnacimiento'];
        $estudiante->cedula = $_REQUEST['cedula'];
        $estudiante->calle = $_REQUEST['calle'];
        $estudiante->ciudad = $_REQUEST['ciudad'];
        $estudiante->telefono = $_REQUEST['telefono'];
        $estudiante->correo = $_REQUEST['correo'];
        $estudiante->contrasena = $_REQUEST['contrasena'];
        $estudiante->calletrabajo = $_REQUEST['calletrabajo'];
        $estudiante->ciudadtrabajo = $_REQUEST['ciudadtrabajo'];
        $estudiante->telefonotrabajo = $_REQUEST['telefonotrabajo'];
        $estudiante->correotrabajo = $_REQUEST['correotrabajo'];
        $estudiante->universidad = $_REQUEST['universidad'];
        $estudiante->licenciatura = $_REQUEST['licenciatura'];
        $estudiante->modalidad = $_REQUEST['modalidad'];
        $estudiante->otrosestudios = $_REQUEST['otrosestudios'];        
        
        $estudiante->id > 0
            ? $this->model->Actualizar($estudiante)
            : $this->model->Registrar($estudiante);

        require_once 'views/estudiante/index.php';
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        require_once 'views/estudiante/index.php';
    }


}