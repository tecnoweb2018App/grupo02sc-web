<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 15-11-18
 * Time: 08:16 PM
 */

require_once 'models/conferencia.php';


class conferenciaController
{
    public function __CONSTRUCT(){
        $this->model = new conferencias();
    }


    public function Index(){
        require_once 'views/conferencia/index.php';

    }

    public function Crud(){
        $conferencias = new conferencias();

        if(isset($_REQUEST['id'])){
            $conferencias = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'views/conferencia/grud.php';

    }


    public function Guardar(){
        $conferencias = new conferencias();

        $conferencias->id = $_REQUEST['id'];
        $conferencias->titulo = $_REQUEST['titulo'];
        $conferencias->descripcion = $_REQUEST['descripcion'];
        $conferencias->disertante = $_REQUEST['disertante'];
        $conferencias->fecha = $_REQUEST['fecha'];
        $conferencias->hora = $_REQUEST['hora'];

        $conferencias->id > 0
            ? $this->model->Actualizar($conferencias)
            : $this->model->Registrar($conferencias);

        require_once 'views/conferencia/index.php';
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        require_once 'views/conferencia/index.php';
    }

    public function conferencias(){
        require_once 'views/conferencia/conferencias.php';
    }

}