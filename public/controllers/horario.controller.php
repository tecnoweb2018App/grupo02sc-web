<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 15-11-18
 * Time: 08:16 PM
 */

require_once 'models/horario.php';


class horarioController
{
    public function __CONSTRUCT(){
        $this->model = new horario();
    }


    public function Index(){
        require_once 'views/horario/index.php';

    }

    public function Crud(){
        $horario = new horario();

        if(isset($_REQUEST['id'])){
            $horario = $this->model->Obtener($_REQUEST['id']);
        }

        require_once 'views/horario/grud.php';

    }


    public function Guardar(){
        $horario = new horario();

        $horario->id = $_REQUEST['id'];
        $horario->dia = $_REQUEST['dia'];
        $horario->horarioinicial = $_REQUEST['horarioinicial'];
        $horario->horariofinal = $_REQUEST['horariofinal'];
       
        $horario->id > 0
            ? $this->model->Actualizar($horario)
            : $this->model->Registrar($horario);

        require_once 'views/horario/index.php';
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        require_once 'views/horario/index.php';
    }


}